# Recherche d'information

L'activité habituelle de recherche d'information est un peu différente de l'habitude : on décompose en 3 parties pour s'imprégner doucement du monde de _l'apprentissage par renforcement_.

L'activité est à réaliser en binôme ou trinôme au choix.

## Partie 1 : "renfor-quoi" ?

Dans un premier temps, vous devez comprendre les différences principales entre les 3 types d'apprentissage machine que vous connaissez :

- Apprentissage supervisé
- Apprentissage non-supervisé
- Apprentissage par renforcement

Illustrez les différences avec un schéma montrant pour chaque type d'apprentissage :

- La forme des données
- L'objectif recherché

## Partie 2 : "agent spécial"

Dans un second temps, vous devez vous attarder sur _l'apprentissage par renforcement_ et comprendre les termes suivants constituant le jargon du domaine :

- Agent
- Environnement
- Action
- Reward (Récompense)
- Policy (Stratégie)
- State (Etat)
- Discount factor (Facteur de pondération)

Illustrez ces concepts par un schéma montrant l'intéraction entre tous ces termes.

## Partie 3 : "tout effort mérite récompense" !

Maintenant que vous avez pris connaissance des concepts de base de l'apprentissage par renforcement, vous devez :

1. Trouver une vidéo sur YouTube utilisant cette technique
2. Identifier chaque concept par rapport au cas d'usage

Présentez votre exemple à la promo.
