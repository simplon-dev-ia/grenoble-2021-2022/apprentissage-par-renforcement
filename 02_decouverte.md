# Découverte de l'apprentissage par renforcement

Pour découvrir le fonctionnement par l'exemple de l'apprentissage par renforcement,
nous allons utiliser une activité imaginée par [AI Unplugged](https://www.aiunplugged.org).
Cette activité reprend le concept d'un jeu d'échec de plateau 3x3 avec des singes et crocodiles.
Les crocodiles sont joués par la machine qui doit apprendre à jouer les bonnes règles pour chaque
coup au fur et à mesure des parties.

Normalement cette activité se fait sans ordinateur avec un plateau, des cartes et des
bonbons mais pour des raisons pratiques nous utiliserons la [version en ligne](https://www.stefanseegerer.de/schlag-das-krokodil/).

## Stratégie d'apprentissage

La version en ligne implémente intégralement les règles du jeu et l'apprentissage
par renforcement de la machine suivant :

- À chaque tour, l'humain joue un coup puis la machine
- La machine joue un coup par identification de la position des pièces pour le tour en cours et des coups associés possibles
- La machine tire aléatoirement un des coups possibles à jouer (matérialisés par les points de couleurs disponibles sur la carte de la position)
- À la fin d'une partie :
    - Si l'humain gagne, on enlève un point de la couleur du coup utilisé au dernier tour par la machine
    - Si la machine gagne, on ajoute un point de la couleur du coup utilisé au dernier tour par la machine

Au fur et à mesure des parties, la machine identifie pour chaque positions possibles les coups qui fonctionnent uniquement et réduit donc son champs de tirage aléatoire à chaque tour pour maximiser ses gains.

**Attention** : cet algorithme d'apprentissage par renforcement est basique mais
fonctionne dans ce cas précis car le jeu est très simple. Il permet surtout d'appréhender
les concepts du renforcement, et d'observer réellement les effets de l'application du concept
de récompense / pénalité.

## Partie 1 : activité individuelle

- Jouer contre la machine en ligne plusieurs parties (minimum 10)
- Observer son apprentissage progressif
- Mettre en lien le jeu avec les concepts du renforcement : agent, environnement, état, action, récompense, stratégie

## Partie 2 : activité en binôme

Trouver combien de tours minimum sont nécessaires à la machine pour apprendre
à être meilleur que 2 personnes humaines ! En d'autres termes : au bout de
combien de parties n'arrivez vous plus à battre la machine?
