![Brain](brain.png)

# Apprentissage par renforcement

> Ou comment la machine apprend sans les règles...

## Lightning Talks

En fin de semaine (vendredi matin), nous allons organiser une session de
[_lightning talks_](https://en.wikipedia.org/wiki/Lightning_talk). Le principe ?
Les présentations s'enchaînent sans pause pendant 1 heure, vous avez 5 minutes à
votre disposition pour nous parler d'un sujet. 5 minutes c'est court, alors soyez
créatifs pour faire passer l'essentiel du message !

Vous aurez du temps de préparation durant la semaine. Ce travail est individuel,
et **le support de présentation sera à rendre sur Simplonline**.

Pour chaque sujet :

- Problème(s) + Solution(s) + Cas d'usage
- Mentionner les sources utilisées
- Mentionner les ressources pour aller plus loin

| Sujet                                       | Apprenant.e |
| ------------------------------------------- | ----------- |
| Equation de Bellman                         | Rayane      |
| Alpha Go et le renforcement                 | Thienvu     |
| Feux de circulation et renforcement         | Marouan     |
| Vehicules autonomes et renforcement         | Aïssa       |
| Systèmes de recommendation et renforcement  | Cinthya     |
| Automatisation industrielle et renforcement | Nolan       |
| La programmation dynamique                  | Gabriel     |
| L'histoire du renforcement                  | Armand      |
| Les algorithmes génétiques                  | Merouane    |

## Ressources

- [MIT 6.S191: Reinforcement Learning](https://www.youtube.com/watch?v=-WbN61qtTGQ)
